import login.Credencial;
import login.Crypto;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Clase encargada de gestionar las peticiones de logeo y confirmación de contraseña
 */
public class Servidor {
    private String clave = "claveChunguisima";
    private ServerSocket socketServidor;
    private int puerto;
    Crypto crypto = new Crypto();
    Credencial credencial;

    /**
     * Constructor del servidor
     * @param puerto
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public Servidor(int puerto) throws NoSuchPaddingException, NoSuchAlgorithmException, IOException {
        this.puerto = puerto;
        credencial = new Credencial();
        arrancarEscucha();
    }

    /**
     * Método que inicia los sockets de entrada y salida, permaneciendo a la escucha
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    private void arrancarEscucha() throws IOException {

        try {
            socketServidor = new ServerSocket(this.puerto);

            while (true) {
                Socket clienteconectado = socketServidor.accept();
                DataOutputStream salida = new DataOutputStream(clienteconectado.getOutputStream());
                String nick;
                String contrasena;
                DataInputStream entrada = new DataInputStream(clienteconectado.getInputStream());

                nick = crypto.desencriptado(clave, entrada.readUTF());
                credencial.setNick(nick);


                contrasena = crypto.desencriptado(clave, entrada.readUTF());
                credencial.setContrasena(contrasena);

                iniciarConsulta(salida);
                entrada.close();
                salida.close();
                clienteconectado.close();
            }
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que se encarga de consultar en fichero los datos enviados de usuario y password para verificación
     * @param salida es el canal de salida para devolver el resultado
     */
    private void iniciarConsulta(DataOutputStream salida) {
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader("users.csv"));
            String linea = reader.readLine();

            while (null != linea) {
                String[] campos = linea.split(";");
                campos = limpiar(campos);
                linea = reader.readLine();
                if ((credencial.getNick()).equals(campos[0])) {
                    if (crypto.verificarHash(campos[1], credencial.getContrasena())) {
                        salida.writeUTF(crypto.encriptacion(clave,
                                String.valueOf(crypto.verificarHash(campos[1], credencial.getContrasena()))));
                        salida.writeUTF(crypto.encriptacion(clave, "usuario"));
                    } else
                        salida.writeUTF(crypto.encriptacion(clave,
                                String.valueOf(crypto.verificarHash(campos[1], credencial.getContrasena()))));
                }
            }

            reader = new BufferedReader(new FileReader("admin.csv"));
            linea = reader.readLine();

            while (null != linea) {
                String[] campos = linea.split(";");
                campos = limpiar(campos);
                linea = reader.readLine();
                if ((credencial.getNick()).equals(campos[0])) {
                    if (crypto.verificarHash(campos[1], credencial.getContrasena())) {
                        salida.writeUTF(crypto.encriptacion(clave,
                                String.valueOf(crypto.verificarHash(campos[1], credencial.getContrasena()))));
                        salida.writeUTF(crypto.encriptacion(clave, "root"));
                    } else
                        salida.writeUTF(crypto.encriptacion(clave,
                                String.valueOf(crypto.verificarHash(campos[1], credencial.getContrasena()))));
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método para limpiar de caracteres propios de un archivo csv que devuelve las palabras limpias
     * @param campos
     * @return
     */
    private String[] limpiar(String[] campos) {
        String palabras[] = new String[campos.length];
        for (int i = 0; i < palabras.length; i++) {
            palabras[i] = campos[i].replaceAll("\"\t", "").replaceAll("\"\t", "")
                    .replaceAll(",", ".").replaceAll("\"", "");
        }
        return palabras;
    }
}
