package login;

public class Credencial {
    String nick;
    String contrasena;

    public Credencial(){
        this.nick="";
        this.contrasena="";
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}